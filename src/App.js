import 'bootstrap/dist/css/bootstrap.min.css';
import Film from './container/Film';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AddFilm from './container/AddFilm';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/film' exact component={Film}/>
        <Route path='/Add' exact component={AddFilm}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
