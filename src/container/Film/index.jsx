import Card from "../../component/Card"
import {Raya,Godzila,Vivo} from "../../assets/img"
import axios from "axios"
import React, { useEffect, useState } from "react"
const Film = () => {
    const [data, setData] = useState([])
    const BASE_URL = 'https://api.themoviedb.org/3';
    const api = axios.create({baseURL: BASE_URL})
    const api_key = '59880a252596ababc4aee50fce6165d8'
    const getImage = (path) => `https://image.tmdb.org/t/p/w300/${path}`

    const getUpcoming = api.get("movie/upcoming",{params: {api_key}})

    useEffect(() => {
        getUpcoming.then((res) => {
            setData(res.data.results);
            console.log(res)
          })
    }, [])
    // const setFilm = [
    //     {
    //         title: 'Raya and The Last Dragon',
    //         desc : 'Raya and the Last Dragon is a 2021 American computer-animated fantasy action-adventure film that was produced by Walt Disney Animation Studios',
    //         image: Raya
    //     },
    //     {
    //         title: 'Godzilla vs. Kong',
    //         desc : 'Godzilla vs. Kong is a 2021 American monster film directed by Adam Wingard. A sequel to Kong: Skull Island (2017) and Godzilla: King of the Monsters (2019)',
    //         image: Godzila
    //     }
    //     ,
    //     {
    //         title: 'Vivo',
    //         desc : 'Vivo is a 2021 American computer-animated musical comedy-drama film produced by Sony Pictures Animation.',
    //         image: Vivo
    //     }]
    return (
        <div className="row">
            {data.map(element =>{
                return(
                    <Card
                    title = {element.original_title}
                    text = {element.overview}
                    img = {getImage(element.poster_path)}
                    />
                )
            })}
        </div>
    )
}
export default Film