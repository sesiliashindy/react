import "../../assets/css/add.css"

const AddFilm = () => {
    return(
        <form className="add-film">
            <div className="mb-3">
                <label for="exampleFormControlInput1" className="form-label">Title</label>
                <input type="text" required="required" className="form-control" id="exampleFormControlInput1" placeholder="Film Title"/>
            </div>
            <div className="mb-3">
                <label for="exampleFormControlTextarea1" className="form-label">Description</label>
                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" required="required"></textarea>
            </div>
            <button type="onSubmit"className="btn btn-primary">Add</button>
        </form>
    )
}
export default AddFilm