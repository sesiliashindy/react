import { Link } from "react-router-dom"
import "../../assets/css/card.css"
const Card = (props) => {
    return(
        <div className="col-sm-3">
            <div className="card">
            <img src={props.img} className="card-img-top" alt={props.img}/>
                <div className="card-body">
                    <h5 className="card-title">{props.title}</h5>
                    <p className="card-text">{props.text}</p>
                    <Link to="#" className="btn btn-primary">See More</Link>
                </div>
            </div>
        </div>
    )
}
export default Card